<?php

/**
 * @author Никонов Владимир Андреевич
 */
class AjaxProductsPage extends AbstractSitePage
{

    /**
     * @return void
     */
    public function process()
    {
        if (Request::isEmpty('request')) {
            exit('0');
        }

        $this->_processRequest();
    }

    /**
     * @return void
     */
    private function _processRequest()
    {
        switch (Request::get('request')) {
            case 'get_products':
                $this->_getProducts();
            default:
                exit('0');
        }
    }

    /**
     *
     * @return void
     */
    private function _getProducts()
    {
        if (!Request::issetParam('page_num') || !Request::issetParam('limit')) {
          exit('0');
        }

        $page = Request::get('page_num');
        $limit = Request::get('limit');
        $pm = new ProductManager();
        $products = $pm->getAll('state=1', array(), $page, $limit);
        if (empty($products)) {
            exit('0');
        }
        exit(json_encode($this->_getPatchedProducts($products)));
    }

    /**
     * @param Product[] $products
     * @return Product[]
     */
    private function _getPatchedProducts($products) {
      $patchedItems = array();
      foreach ($products AS $product) {
        array_push($patchedItems, ProductBuilder::patchItem($product));
      }
      return $patchedItems;
    }

    public function show()
    {

    }

}
