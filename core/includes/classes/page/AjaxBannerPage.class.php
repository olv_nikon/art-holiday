<?php

/**
 * @author Никонов Владимир Андреевич
 */
class AjaxBannerPage extends AbstractSitePage
{

    /**
     * @return void
     */
    public function process()
    {
        if (Request::isEmpty('request')) {
            exit('0');
        }

        $this->_processRequest();
    }

    /**
     * @return void
     */
    private function _processRequest()
    {
        switch (Request::get('request')) {
            case 'get_banner':
                $this->_getBanner();
            default:
                exit('0');
        }
    }

    /**
     *
     * @return void
     */
    private function _getBanner()
    {
        $bm = new BannerManager();
        $banner = $bm->getAll('', array(), '', 1);
        if (empty($banner)) {
            exit('0');
        }
        exit(json_encode($banner[0]));
    }

    public function show()
    {

    }

}
