<?php

/**
 * @author Никонов Владимир Андреевич
 */
class SlideView
{

    /**
     * Show slides
     *
     * @param Template $tpl
     * @param Slide[] $slides
     * @return string HTML
     */
    public static function getHTML(Template $tpl, $slides)
    {
        if (empty($slides)) {
            return '';
        }

        foreach ($slides AS $slide) {
            $tpl->setVar('Slide-Img', $slide->url);
            $tpl->parseB2V('Slides', 'SLIDE', TRUE);
        }

        return $tpl->fillTemplate();
    }

}
