<?php

/**
 * @author Никонов Владимир Андреевич
 */
class BadgeView
{

    /**
     * Show badges
     *
     * @param Template $tpl
     * @param Badge[] $badges
     * @return string HTML
     */
    public static function getHTML(Template $tpl, $badges)
    {
        if (empty($badges)) {
            return '';
        }

        foreach ($badges AS $badge) {
            $tpl->setVar('Badge-Image', $badge->image);
            $tpl->setVar('Badge-Caption', $badge->caption);
            $tpl->setVar('Badge-Content', $badge->content);
            $tpl->parseB2V('Badges', 'BADGE', TRUE);
        }

        return $tpl->fillTemplate();
    }

}
