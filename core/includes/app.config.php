<?php

return array(
    'STATIC_FILE_VERSION' => 13,
    'SETTING_MANAGER' => 'SettingManager',
    'SECURITY_MANAGER' => 'SecurityFuncs',
    'ITEMS_PER_PAGE' => 10
);
