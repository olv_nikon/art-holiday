<?php

/**
 * @author Никонов Владимир Андреевич
 */
class Banner extends AbstractEntity
{

    public $id;
    public $name;
    public $content;
    public $image;
    public $state;
    public $modifyDate;
    public $user;
    public $sortOrder;

}

class BannerManager extends AbstractManager
{

}
