<?php

/**
 * @author Никонов Владимир Андреевич
 */
class Badge extends AbstractEntity
{

    public $id;
    public $image;
    public $caption;
    public $content;
    public $user;
    public $modifyDate;
    public $sortOrder;

}

class BadgeManager extends AbstractManager
{

}
