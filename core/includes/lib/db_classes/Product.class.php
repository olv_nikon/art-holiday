<?php

/**
 * @author Никонов Владимир Андреевич
 */
class Product extends AbstractEntity
{

    public $id;
    public $caption;
    public $url;
    public $description;
    public $state;
    public $category;
    public $isSpecial;
    public $price;
    public $newPrice;
    public $hasHiFloat;
    public $metaKeywords;
    public $metaDescription;
    public $createDate;
    public $modifyDate;
    public $sortOrder;
    public $user;

}

class ProductManager extends AbstractManager
{

}
