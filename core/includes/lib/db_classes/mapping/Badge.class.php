<?php

/**
 * @author Никонов Владимир Андреевич
 */
class _Badge
{

    public $entityName = 'Badge';
    public $tableName = 'badge';
    public $mapping = array(
        'id' => 'id',
        'image' => 'image',
        'caption' => 'caption',
        'content' => 'content',
        'user' => 'user',
        'modifyDate' => 'modify_date',
        'sortOrder' => 'sort_order'
    );
    public $types = array(
        'id' => 'TEXT',
        'image' => 'TEXT',
        'caption' => 'TEXT',
        'content' => 'TEXT',
        'user' => 'TEXT',
        'modifyDate' => 'TEXT',
        'sortOrder' => 'TEXT'
    );
    public $order = 'ORDER BY sort_order ASC';
    public $group = '';

}
