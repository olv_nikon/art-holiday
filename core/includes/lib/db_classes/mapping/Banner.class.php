<?php

/**
 * @author Никонов Владимир Андреевич
 */
class _Banner
{

    public $entityName = 'Banner';
    public $tableName = 'product__banner';
    public $mapping = array(
        'id' => 'id',
        'name' => 'name',
        'content' => 'content',
        'image' => 'image',
        'state' => 'state',
        'modifyDate' => 'modify_date',
        'user' => 'user',
        'sortOrder' => 'sort_order'
    );
    public $types = array(
        'id' => 'TEXT',
        'name' => 'TEXT',
        'content' => 'TEXT',
        'image' => 'TEXT',
        'state' => 'TEXT',
        'modifyDate' => 'TEXT',
        'user' => 'TEXT',
        'sortOrder' => 'TEXT'
    );
    public $order = 'ORDER BY sort_order ASC';
    public $group = '';

}
