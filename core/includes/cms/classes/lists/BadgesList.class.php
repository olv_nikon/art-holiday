<?php

use Lists\Dictionary\Field;

/**
 * @author Никонов Владимир Андреевич
 */
class BadgesList extends AbstractList
{

    protected $_canRemove = TRUE;
    protected $_canAdd = TRUE;
    protected $_sortable = TRUE;
    protected $_entityName = 'Badge';

    protected function _getFields()
    {
        return array(
            'caption' => array(Field::CAPTION, 'Подпись', TRUE),
        );
    }

}
