<?php

/**
 * @author Никонов Владимир Андреевич
 */
class BadgesCmsPage extends AbstractSimpleCmsPage
{

    /**
     *
     * @var string Хлебные крошки
     */
    protected $_breadTrails = 'Баджи';
    protected $_hasPaginator = FALSE;

    /**
     *
     * @return \BadgesCmsPage
     */
    protected function _initManager()
    {
        $this->_manager = new BadgeManager();
        return parent::_initManager();
    }

    /**
     * Инициализация листа
     *
     * @return \BadgesCmsPage
     */
    protected function _initList()
    {
        $this->_list = new BadgesList();
        return parent::_initList();
    }

    /**
     * Инициализация редактора
     *
     * @return \BadgeEditor
     */
    protected function _getEditor($item)
    {
        return new BadgeEditor($item);
    }

}
