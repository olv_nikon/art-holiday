<?php

use Editor\Dictionary\Field;

/**
 * @author Никонов Владимир Андреевич
 */
class BadgeEditor extends AbstractEditor
{

    protected $_requiredFields = array('caption');
    protected $_entityName = 'Badge';

    protected function _getControls()
    {
        return array(
            'caption' => array(Field::TEXT, 'Подпись'),
            'image' => array(Field::IMAGE, 'Изображение'),
            'content' => array(Field::TEXTEDITOR, 'Текст')
        );
    }

}
