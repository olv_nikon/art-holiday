<?php

use Editor\Dictionary\Field;

/**
 * @author Никонов Владимир Андреевич
 */
class StaticPageEditor extends AbstractEditor
{

    protected $_entityName = 'StaticPage';

    protected function _getControls()
    {
        return array(
            'uploader' => array(Field::FILEUPLOADER, 'Ресурсы'),
            'content' => array(Field::TEXTEDITOR, 'Содержимое')
        );
    }

}
