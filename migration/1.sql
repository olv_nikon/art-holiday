# Новые настройки
INSERT INTO `setting`
(`id`, `param_name`, `param_value`, `type`, `modify_date`, `user`, `sort_order`)
VALUES
(NULL, 'Цена Hi-Float', '{"paramValue":"50"}', 1, NULL, NULL, NULL),
(NULL, 'Описание Hi-Float', '{"paramValue":"Позволяет воздушным шарам летать очень высоко."}', 3, NULL, NULL, NULL);

# Новая страница
INSERT INTO `page`
(`id`, `name`, `class`, `is_cms`, `settings`)
VALUES
(NULL, 'promotions', 'BannerCmsPage', '1', NULL);

# Новая сущность
CREATE TABLE `product__banner` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `state` tinyint(2) UNSIGNED NOT NULL,
  `modify_date` datetime NOT NULL,
  `user` int(11) UNSIGNED DEFAULT NULL,
  `sort_order` int(11) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `product__banner`
ADD PRIMARY KEY (`id`);

ALTER TABLE `product__banner`
MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
