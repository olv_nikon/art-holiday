# Страницы для подгрузки баннера и товаров
INSERT INTO `page`
(`id`, `name`, `class`, `is_cms`, `settings`)
VALUES
(NULL, 'ajax_banner', 'AjaxBannerPage', 0, NULL),
(NULL, 'ajax_products', 'AjaxProductsPage', 0, NULL);

# Ролик на YouTube
INSERT INTO `setting` (`id`, `param_name`, `param_value`, `type`, `modify_date`, `user`, `sort_order`) VALUES
(NULL, 'Картинка ролика YouTube', '{"paramValue":""}', 8, NULL, NULL, NULL),
(NULL, 'Ссылка на ролик YouTube', '{"paramValue":""}', 1, NULL, NULL, NULL);
