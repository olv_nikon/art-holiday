-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 23, 2017 at 08:20 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `artholiday`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `c_hash` varchar(255) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  `settings` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `is_cms` tinyint(1) UNSIGNED DEFAULT NULL,
  `settings` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `name`, `class`, `is_cms`, `settings`) VALUES
(1, 'index', 'MainPage', 0, NULL),
(2, 'article', 'ArticlePage', 0, NULL),
(3, 'contacts', 'ContactsPage', 0, NULL),
(4, 'reviews', 'ReviewsPage', 0, NULL),
(5, 'send_review', 'SendReviewPage', 0, NULL),
(6, 'send_email', 'SendEmailPage', 0, NULL),
(8, 'content_editor', 'ContentEditorCmsPage', 1, NULL),
(19, 'products', 'ProductCmsPage', 1, NULL),
(10, 'settings', 'SettingsCmsPage', 1, NULL),
(11, 'users', 'UserCmsPage', 1, NULL),
(13, 'resources', 'ResourceCmsPage', 1, NULL),
(14, 'autorization', 'AutorizationCmsPage', 1, NULL),
(15, 'exit', 'ExitCmsPage', 1, NULL),
(16, 'file_upload', 'FileUploadCmsPage', 1, NULL),
(22, 'categories', 'CategoriesCmsPage', 1, NULL),
(24, 'purchases', 'PurchasesCmsPage', 1, NULL),
(25, 'catalog', 'CatalogPage', 0, NULL),
(26, 'product', 'ProductPage', 0, NULL),
(27, 'about', 'AboutUsPage', 0, NULL),
(28, 'discount', 'DiscountPage', 0, NULL),
(29, 'delivery', 'DeliveryPage', 0, NULL),
(30, 'shops', 'ShopsPage', 0, NULL),
(31, 'bigbuy', 'BigBuyPage', 0, NULL),
(32, 'collective', 'CollectiveBuyPage', 0, NULL),
(33, 'slider', 'SliderCmsPage', 1, NULL),
(34, 'login', 'LoginPage', 0, NULL),
(37, 'account', 'AccountPage', 0, NULL),
(39, 'registration', 'RegistrationPage', 0, NULL),
(40, 'verification', 'VerificationPage', 0, NULL),
(41, 'forgot', 'ForgotPage', 0, NULL),
(42, 'ajax_basket', 'AjaxBasketPage', 0, NULL),
(43, 'cart', 'CartPage', 0, NULL),
(44, 'purchase', 'PurchasePage', 0, NULL),
(47, 'payment_success', 'PaymentSuccessPage', 0, NULL),
(48, 'payment_error', 'PaymentErrorPage', 0, NULL),
(49, 'payment_proceed', 'PaymentProceedPage', 0, NULL),
(50, 'payment_process', 'PaymentProcessPage', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) UNSIGNED NOT NULL,
  `caption` varchar(255) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `description` text,
  `state` tinyint(2) UNSIGNED NOT NULL,
  `category` int(11) UNSIGNED NOT NULL,
  `is_special` int(2) UNSIGNED NOT NULL,
  `price` varchar(10) NOT NULL,
  `new_price` varchar(10) DEFAULT NULL,
  `settings` text,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `sort_order` int(11) UNSIGNED DEFAULT NULL,
  `user` int(11) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product__category`
--

CREATE TABLE `product__category` (
  `id` int(11) UNSIGNED NOT NULL,
  `caption` varchar(255) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `state` int(11) UNSIGNED NOT NULL,
  `parent_id` int(11) UNSIGNED NOT NULL,
  `featured` int(1) UNSIGNED DEFAULT NULL,
  `sort_order` int(11) UNSIGNED NOT NULL,
  `settings` text,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `user` int(11) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product__photo`
--

CREATE TABLE `product__photo` (
  `id` int(11) UNSIGNED NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `sort_order` int(11) UNSIGNED NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `user` int(11) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

CREATE TABLE `purchase` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `state` int(2) UNSIGNED NOT NULL,
  `total` double NOT NULL,
  `comment` text,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `settings` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `resource`
--

CREATE TABLE `resource` (
  `id` int(11) NOT NULL,
  `url` varchar(200) NOT NULL,
  `create_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `param_name` varchar(200) NOT NULL,
  `param_value` text NOT NULL,
  `type` int(1) NOT NULL,
  `modify_date` datetime DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `sort_order` int(11) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `param_name`, `param_value`, `type`, `modify_date`, `user`, `sort_order`) VALUES
(19, 'Телефон для связи (шапка)', '{"paramValue":"+7-926-696-00-88"}', 1, '2016-07-03 20:42:23', 1, 10),
(3, 'Текст подвала', '{"paramValue":"\\u00a92016 \\u0412\\u0441\\u0435 \\u043f\\u0440\\u0430\\u0432\\u0430 \\u0437\\u0430\\u0449\\u0438\\u0449\\u0435\\u043d\\u044b \\u0424\\u0435\\u0440\\u043c\\u0430\\u043d\\u0435\\u043f\\u0435\\u0446\\u0438\\u043d\\u043e "}', 3, '2016-10-07 20:42:52', 1, 130),
(4, 'Статус сайта', '{"paramValue":"1"}', 5, '2016-07-28 12:16:55', 1, 40),
(5, 'Название сайта (title)', '{"paramValue":"\\u0425\\u043e\\u0440\\u043e\\u0448\\u0438\\u0439 \\u0432\\u043a\\u0443\\u0441 - \\u043d\\u0430\\u0441\\u0442\\u043e\\u044f\\u0449\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0442\\u044b"}', 1, '2016-07-03 20:42:23', 1, 60),
(6, 'Мета-данные: описание (description)', '{"paramValue":"\\u0424\\u0435\\u0440\\u043c\\u0435\\u0440\\u0441\\u043a\\u043e\\u0435 \\u043c\\u044f\\u0441\\u043e \\u0438 \\u043f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0442\\u044b \\u0441 \\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u043e\\u0439 \\u043d\\u0430 \\u0434\\u043e\\u043c \\u0432 \\u041a\\u043e\\u043b\\u043e\\u043c\\u043d\\u0435"}', 3, '2016-09-13 14:51:47', 1, 90),
(7, 'Мета-данные: ключевые слова (keywords)', '{"paramValue":"\\u0422\\u043e\\u0432\\u0430\\u0440\\u044b, \\u0422\\u043e\\u0432\\u0430\\u0440\\u044b \\u0432 \\u041a\\u043e\\u043b\\u043e\\u043c\\u043d\\u0435, \\u043f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0442\\u044b \\u0432 \\u041a\\u043e\\u043b\\u043e\\u043c\\u043d\\u0435"}', 3, '2016-09-13 14:50:59', 1, 80),
(10, 'Логотип (шапка) в формате png', '{"paramValue":"img\\/logo.png"}', 8, '2016-07-03 20:42:23', 1, 100),
(22, 'Правый баннер главной страницы', '{"paramValue":"images\\/3db6fcd1e80408923ca0ff4d7f7edb9a.jpg"}', 8, '2016-07-07 18:55:08', 1, 110),
(23, 'Ссылка правого баннера главной страницы', '{"paramValue":"http:\\/\\/fermanepecino.ru\\/product\\/farsh-govyazhiy.html"}', 1, '2016-09-13 15:06:55', 1, 120),
(14, 'Email для получения писем с сайта', '{"paramValue":"kobyteva21@yandex.ru"}', 1, '2016-09-13 14:50:21', 1, 50),
(18, 'Время работы (шапка)', '{"paramValue":"\\u0417\\u0430\\u043a\\u0430\\u0437\\u044b \\u043f\\u0440\\u0438\\u043d\\u0438\\u043c\\u0430\\u044e\\u0442\\u0441\\u044f \\u0441 9:00 \\u0434\\u043e 18:00 \\u0435\\u0436\\u0435\\u0434\\u043d\\u0435\\u0432\\u043d\\u043e. \\u0414\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430 \\u043e\\u0441\\u0443\\u0449\\u0435\\u0441\\u0442\\u0432\\u043b\\u044f\\u0435\\u0442\\u0441\\u044f \\u0441 18:00 \\u0434\\u043e 20:00 \\u0435\\u0436\\u0435\\u0434\\u043d\\u0435\\u0432\\u043d\\u043e"}', 1, '2016-09-14 11:24:32', 1, 20),
(20, 'Адрес (шапка)', '{"paramValue":"\\u0441. \\u041d\\u0435\\u043f\\u0435\\u0446\\u0438\\u043d\\u043e, \\u041c\\u043e\\u0441\\u043a\\u043e\\u0432\\u0441\\u043a\\u0430\\u044f \\u043e\\u0431\\u043b\\u0430\\u0441\\u0442\\u044c, \\u041a\\u043e\\u043b\\u043e\\u043c\\u0435\\u043d\\u0441\\u043a\\u0438\\u0439 \\u0440\\u0430\\u0439\\u043e\\u043d"}', 1, '2016-09-13 14:52:05', 1, 30),
(25, 'Код карты обратной связи', '{"paramValue":"<iframe src=\\"https:\\/\\/www.google.com\\/maps\\/embed?pb=!1m14!1m12!1m3!1d59394.75720093041!2d2.3728539744224704!3d48.86007484767876!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2sru!4v1467567775794\\" width=\\"600\\" height=\\"450\\" frameborder=\\"0\\" style=\\"border:0\\" allowfullscreen><\\/iframe>"}', 3, '2016-07-03 20:43:06', 1, 140),
(26, 'Стоимость доставки курьером', '{"paramValue":"250"}', 1, '2016-07-03 20:42:23', 1, 70),
(27, 'Мы в контакте', '{"paramValue":"https:\\/\\/vk.com\\/farmnepecino"}', 1, '2017-01-28 15:46:25', 1, 150),
(28, 'Мы в однокласниках', '{"paramValue":"https:\\/\\/ok.ru\\/group\\/53344914571446"}', 1, '2017-01-28 15:46:35', 1, 160),
(29, 'Мы на facebook', '{"paramValue":"https:\\/\\/www.facebook.com\\/profile.php?id=100011885499942&amp;ref=bookmarks"}', 1, '2017-01-28 15:46:44', 1, 170),
(30, 'Мы на инстаграм', '{"paramValue":"http:\\/\\/instagram.com\\/fermanepecino"}', 1, '2017-01-28 15:46:53', 1, 180);

-- --------------------------------------------------------

--
-- Table structure for table `slide`
--

CREATE TABLE `slide` (
  `id` int(11) NOT NULL,
  `url` varchar(200) NOT NULL,
  `sort_order` int(11) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `static_page`
--

CREATE TABLE `static_page` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `modify_date` datetime DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `settings` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `static_page`
--

INSERT INTO `static_page` (`id`, `name`, `content`, `modify_date`, `user`, `settings`) VALUES
(1, 'Почему именно мы', '<p><strong><span style="font-family: tahoma, arial, helvetica, sans-serif; font-size: large;">С нами надёжно.</span></strong></p>\r\n<p><span style="font-family: tahoma, arial, helvetica, sans-serif; font-size: large;">&nbsp; <span style="font-size: medium;">&nbsp; Крестьянское фермерское хозяйство <span style="font-size: large;">"Непецино"</span> на рынке мясных продуктов работает уже более трёх лет. На протяжении этого времени о<span style="line-height: 16.5px;">сновным профилем нашей деятельности было снабжение пищевых производств, ресторанов, региональных оптовых покупателей мясом и изделиями из мяса.</span></span></span></p>\r\n<p>&nbsp;</p>\r\n<p><strong><span style="font-family: tahoma, arial, helvetica, sans-serif; font-size: large;"><span style="line-height: 16.5px;">С нами вкусно.</span></span></strong></p>\r\n<p><span style="font-family: tahoma, arial, helvetica, sans-serif; font-size: large;"><span style="line-height: 16.5px;">&nbsp; &nbsp; <span style="font-size: medium;">Н</span><span style="font-size: medium;">аша компания всегда стремится к развитию, и сегодня мы рады представить нашим покупателям широкий ассортимент вкусной и качественной фермерской продукции от нежных колбасок для гриля до ароматной выпечки.&nbsp;</span></span></span></p>\r\n<p>&nbsp;</p>\r\n<p><strong><span style="font-family: tahoma, arial, helvetica, sans-serif; font-size: large;"><span style="line-height: 16.5px;">С нами безопасно.</span></span></strong></p>\r\n<p><span style="font-family: tahoma, arial, helvetica, sans-serif; font-size: large;"><span style="line-height: 16.5px;">&nbsp; <span style="font-size: medium;">&nbsp; Мы тщательно отслеживаем качество нашего мяса и систематически осуществляем строжайший ветеринарный контроль. Делаем всё для того, чтобы каждый продукт, который попадает к Вам на стол, был не только вкусным и свежим, но и безопасным.</span></span></span></p>\r\n<p>&nbsp;</p>\r\n<p><strong><span style="font-family: tahoma, arial, helvetica, sans-serif; font-size: large;"><span style="line-height: 16.5px;">С нами удобно.</span></span></strong></p>\r\n<p><span style="font-family: tahoma, arial, helvetica, sans-serif; font-size: large;"><span style="line-height: 16.5px;">&nbsp; &nbsp; <span style="font-size: medium;">Так почему же именно мы? Наша компания понимает, что качественная вкусная еда - это залог не только хорошего настроения, но и здоровья всей Вашей семьи. И мы постарались сделать всё для того, чтобы деревенские экологически чистые продукты стали доступны в условиях городского ритма жизни. Согласитесь, ведь не всегда есть время на покупку хороших товаров, поэтому очень часто приходится приобретать порой сомнительное мясо, молоко, сыр и т.д. в ближайшем магазине. Но теперь с <span style="font-size: large;">КФХ "Непецино"</span> Вы можете заказать качественную фермерскую продукцию даже не выходя из дома. Просто позвонив по телефону <span style="font-size: large;">+7-926-696-00-88</span> или оформив заявку на сайте. Наши курьеры доставят Вам заказ в удобное для Вас время.&nbsp;</span></span></span><span style="font-family: tahoma, arial, helvetica, sans-serif; font-size: large; line-height: 16.5px;">&nbsp;</span>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><img src="http://fermanepecino.ru/images/30da5359f21f68097ab911e7019b6bbf.jpg" alt="" width="1158" height="250" /></p>', '2016-09-13 14:38:10', 1, '{"keywords":"\\u0421\\u043e\\u0441\\u0438\\u0441\\u043a\\u0438, \\u043a\\u043e\\u043b\\u0431\\u0430\\u0441\\u044b, \\u0441\\u043e\\u0441\\u0438\\u0441\\u043a\\u0438 \\u0438 \\u043a\\u043e\\u043b\\u0431\\u0430\\u0441\\u044b","description":"\\u0422\\u0435\\u0441\\u0442"}'),
(2, 'Доставка и оплата', '<p><span style="font-size: large;"><strong>Чтобы заказать продукцию КФХ "Непецино", нужно оформить покупку на сайте или позвонить по телефону +7-926-696-00-88.</strong></span></p>\r\n<p><span style="font-size: large;"><strong>Доставка осуществляется ежедневно с 18:00 до 20:00.</strong></span></p>\r\n<p><span style="font-size: large;"><strong>Оплатить заказ Вы можете как наличными при получении, так и банковской картой при оформлении заказа на сайте.</strong></span>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><img src="http://fermanepecino.ru/images/b6d36a36d41a8f5c280db0845a570e0e.jpg" alt="" width="1160" height="238" /></p>', '2016-09-13 14:39:49', 1, '{"keywords":"\\u0414\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430, \\u0435\\u0434\\u0430 \\u0441 \\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u043e\\u0439, \\u043c\\u044f\\u0441\\u043e, \\u043a\\u0443\\u043f\\u0430\\u0442\\u044b, \\u0448\\u0430\\u0448\\u043b\\u044b\\u043a\\u0438 \\u0441 \\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u043e\\u0439, \\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430 \\u043f\\u043e\\u043b\\u0443\\u0444\\u0430\\u0431\\u0440\\u0438\\u043a\\u0430\\u0442\\u043e\\u0432, \\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430 \\u043f\\u0438\\u0440\\u043e\\u0433\\u043e\\u0432.","description":"\\u0414\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430, \\u0435\\u0434\\u0430 \\u0441 \\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u043e\\u0439, \\u043c\\u044f\\u0441\\u043e \\u0441 \\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u043e\\u0439"}'),
(4, 'Где купить?', '<p><strong><span style="font-size: large;">Продукцию КФХ "Непецино" Вы можете заказать на сайте fermanepecino.ru. </span></strong></p>\r\n<p><strong><span style="font-size: large;">А также приобрести в розничном магазине на Афганском рынке, павильон №</span></strong></p>\r\n<p>&nbsp;</p>\r\n<p><img src="http://fermanepecino.ru/images/70041dd69066bc668cc5d8180d27421e.jpg" alt="" width="1167" height="274" /></p>', '2016-09-10 15:30:02', 1, '{"keywords":"\\u041a\\u0443\\u043f\\u0438\\u0442\\u044c \\u043c\\u044f\\u0441\\u043e, \\u043f\\u043e\\u043b\\u0443\\u0444\\u0430\\u0431\\u0440\\u0438\\u043a\\u0430\\u0442\\u044b, \\u043f\\u0438\\u0440\\u043e\\u0433\\u0438, \\u0441\\u044b\\u0440, \\u043c\\u0430\\u0441\\u043b\\u043e, \\u043c\\u043e\\u043b\\u043e\\u0447\\u043a\\u0443.","description":"\\u041a\\u0443\\u043f\\u0438\\u0442\\u044c \\u043c\\u044f\\u0441\\u043e, \\u043f\\u043e\\u043b\\u0443\\u0444\\u0430\\u0431\\u0440\\u0438\\u043a\\u0430\\u0442\\u044b, \\u043f\\u0438\\u0440\\u043e\\u0433\\u0438, \\u0441\\u044b\\u0440, \\u043c\\u0430\\u0441\\u043b\\u043e, \\u043c\\u043e\\u043b\\u043e\\u0447\\u043a\\u0443."}'),
(5, 'Скидки', '<p>Скидки</p>', '2016-06-16 00:16:46', 1, '{"keywords":"Мета скидки слова","description":"Мета скидки описание"}'),
(6, 'Большая покупка', '', NULL, NULL, ''),
(7, 'Коллективная покупка', '', NULL, NULL, ''),
(8, 'Аккаунт (Приветствие)', '<p>Вас приветствует КФХ "Непецино". Желаем приятных покупок!</p>', '2016-09-10 11:21:02', 1, '{"keywords":"","description":""}'),
(9, 'Авторизация (Заголовок)', '<p>Войдите в личный кабинет, если он у Вас уже есть, или зарегистрируйтесь на сайте.</p>', '2016-09-10 11:20:06', 1, '{"keywords":"","description":""}'),
(10, 'Авторизация (Новые пользователи)', '<p><span style="color: #7f7f7f; font-family: Arial; font-size: 12px; line-height: 18px;">Чтобы совершить заказ, Вам необходимо зарегистироваться на сайте.</span></p>', '2016-09-13 14:41:50', 1, '{"keywords":"","description":""}'),
(11, 'Авторизация (Зарегистрированные пользователи)', '<p><span style="color: #7f7f7f; font-family: Arial; font-size: 12px; line-height: 18px;">Добро пожаловать!</span></p>', '2016-09-10 11:22:50', 1, '{"keywords":"","description":""}'),
(16, 'Корзина (Блок суммы)', '<p>Если Вы уже зарегистрированы на нашем сайте, то при заказе по телефону все персональные скидки сохраняются.</p>', '2016-09-10 11:32:10', 1, '{"keywords":"","description":""}'),
(12, 'Регистрация (Заголовок)', '<p>Регистрация на сайте</p>', '2016-09-10 11:24:32', 1, '{"keywords":"","description":""}'),
(13, 'Регистрация (Параграф)', '<p>Заполните все поля анкеты, чтобы зарегистрироваться на сайте.</p>', '2016-09-10 11:26:21', 1, '{"keywords":"","description":""}'),
(14, 'Забыли пароль (Заголовок)', '<h1 style="font-size: 22px; line-height: 24px; color: #584c44; font-family: \'Open sans\', sans-serif; text-transform: lowercase; margin: 0px 0px 32px; padding: 0px;">Забыли пароль?</h1>', '2016-09-10 11:28:57', 1, '{"keywords":"","description":""}'),
(15, 'Забыли пароль (Параграф)', '<p><span style="color: #222222; font-family: \'dejavu sans mono\', monospace; font-size: 11px; white-space: pre-wrap;">Введите Ваш E-mail адрес, на который Вам будет отправлен пароль для восстановления аккаунта.</span></p>', '2016-09-10 11:30:47', 1, '{"keywords":"","description":""}'),
(17, 'Обратная связь', '<h3 style="font-size: 15px; color: #333333; margin: -3px 0px 0px; padding: 0px; font-family: Arial; line-height: 16px; text-align: left;">По всем вопросам связаться с нами можно по телефону +7-926-696-00-88.&nbsp;</h3>', '2016-09-10 11:34:03', 1, '{"keywords":"","description":""}'),
(18, 'Успешный платёж', '<p>Платёж прошёл успешно!</p>', '2016-09-10 11:34:11', 1, '{"keywords":"","description":""}'),
(19, 'Ошибка платёжа', '<p>Во время оплаты произошла ошибка!</p>', '2016-09-10 11:34:17', 1, '{"keywords":"","description":""}'),
(20, 'Завершение платежа', '<p>Платёж завершен!</p>', '2016-09-10 11:34:45', 1, '{"keywords":"","description":""}');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `ne_passwd` varchar(200) NOT NULL,
  `type` int(11) NOT NULL,
  `phone` varchar(200) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `town` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `state` int(1) NOT NULL,
  `subscription` int(1) UNSIGNED DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  `logon_date` datetime DEFAULT NULL,
  `settings` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `password`, `ne_passwd`, `type`, `phone`, `name`, `last_name`, `town`, `address`, `state`, `subscription`, `create_date`, `modify_date`, `logon_date`, `settings`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 1, '1', 'Администратор', 'Главный', '', '', 1, 0, '2015-06-03 00:00:00', '2017-09-23 07:50:16', '2017-09-23 07:50:16', '{"priveleges":"","company":"","companyPhone":"","fax":"","postcode":""}');

-- --------------------------------------------------------

--
-- Table structure for table `verification_code`
--

CREATE TABLE `verification_code` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `v_code` varchar(255) NOT NULL,
  `state` int(1) UNSIGNED NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `activation_date` datetime DEFAULT NULL,
  `end_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `c_hash` (`c_hash`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `url` (`url`);

--
-- Indexes for table `product__category`
--
ALTER TABLE `product__category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `url` (`url`);

--
-- Indexes for table `product__photo`
--
ALTER TABLE `product__photo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase`
--
ALTER TABLE `purchase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resource`
--
ALTER TABLE `resource`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `static_page`
--
ALTER TABLE `static_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `verification_code`
--
ALTER TABLE `verification_code`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `v_code` (`v_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product__category`
--
ALTER TABLE `product__category`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `product__photo`
--
ALTER TABLE `product__photo`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `purchase`
--
ALTER TABLE `purchase`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `resource`
--
ALTER TABLE `resource`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `slide`
--
ALTER TABLE `slide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `static_page`
--
ALTER TABLE `static_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `verification_code`
--
ALTER TABLE `verification_code`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
