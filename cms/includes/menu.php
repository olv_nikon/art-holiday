<?php

use \Menu\Element;
use \Menu\API;

require_once 'menu/API.php';
require_once 'menu/Element.php';
$cmsPages = new API();

$pagesEditor = new Element('Редактор', 'page_editors');
$pagesEditor->addChild(new Element('Блоки', 'content_editor'));
$pagesEditor->addChild(new Element('Слайдер', 'slider'));
$pagesEditor->addChild(new Element('Баджи', 'badges'));
$cmsPages->addElement($pagesEditor);

$products = new Element('Товары', 'products_manager');
$products->addChild(new Element('Категории товаров', 'categories'));
$products->addChild(new Element('Список товаров', 'products'));
$products->addChild(new Element('Акции', 'promotions'));
$products->addChild(new Element('Покупки', 'purchases'));
$cmsPages->addElement($products);

$cmsPages->addElement(new Element('Пользователи', 'users'));

$cmsPages->addElement(new Element('Ресурсы', 'resources'));
$cmsPages->addElement(new Element('Настройки', 'settings'));

// Сервисные страницы
$cmsPages->addElement(
    new Element('Авторизация', API::AUTORIZATION, TRUE)
);
$cmsPages->addElement(
    new Element('Выход', API::LOGOUT, TRUE)
);
$cmsPages->addElement(
    new Element('Загрузка файлов', API::FILE_UPLOAD, TRUE)
);

$GLOBALS['cmsPages'] = $cmsPages;
