(function ($) {
    jQuery.fn.openEditor = function () {
        var options = {
            editor: '#editor',
            listPage: '.bl_table_catalog',
            editorPage: '.bl_redactirovat'
        };
        var methods = {
            init: function () {
                $.ajax({
                    type: "POST",
                    url: "",
                    data: {
                        idItem: $(this).attr('alt')
                    },
                    success: methods._openEditor
                });
                return false;
            },
            _openEditor: function (data) {
                $(options.editor).html(data);
                $(options.listPage).fadeOut(500, function () {
                    $(options.editorPage).fadeIn(500);
                });
            }
        };

        $(this).on('click', methods.init);
    };

    jQuery.fn.removeItem = function () {
        var methods = {
            init: function () {

                if (window.confirm(
                    'Вы действительно хотите выполнить удаление?')) {
                    methods._removeItem($(this));
                }
                return false;
            },
            _removeItem: function (_this) {
                var tr = _this.closest('tr');
                $.ajax({
                    type: "POST",
                    url: "",
                    data: {
                        request: 'remove',
                        idItemRemove: _this.attr('alt')
                    },
                    success: function () {
                        tr.hide(500);
                    }
                });
            }
        };

        $(this).on('click', methods.init);
    };

    jQuery.fn.sendSubscription = function () {
        var methods = {
            init: function () {
                if (window.confirm(
                    'Вы действительно хотите отправить рассылку?')) {
                    methods._send($(this));
                }
                return false;
            },
            _send: function (_this) {
                var state = _this.closest('tr').find('.td_state img');
                $.ajax({
                    type: "POST",
                    url: "",
                    data: {
                        request: 'subscription',
                        idItemSubscription: _this.attr('alt')
                    },
                    success: function () {
                        state.attr('src', 'images/status2.png');
                        alertify.success('Рассылка отправлена!');
                    }
                });
            }
        };

        $(this).on('click', methods.init);
    };
})(jQuery);

$(document).ready(function () {
    $('.act-edit, .act-add').openEditor();
    $('.act-remove').removeItem();
    $('.act-send').sendSubscription();
});

$(document).on('click', '.act-multyremove', function () {
    var removeIds = [];
    $('.multy-remove').each(function (i, item) {
        if ($(item).is(':checked')) {
            removeIds.push($(item).val());
        }
    });

    if (window.confirm('Вы действительно хотите выполнить удаление?')) {
        $.ajax({
            type: "POST",
            url: "",
            data: {
                request: 'remove',
                'idItemRemove[]': removeIds
            },
            success: function () {
                removeIds.forEach(function (item, i) {
                    $('.multy-remove[value="' + item + '"]').closest('tr').
                        remove();
                });
            }
        });
    }
    return false;
});

$(document).on('click', '.ic_but[title="Редактировать слайдер"]', function () {
    $('.bl_table_catalog').fadeOut(500, function () {
        $('.bl_redactirovat_slider').fadeIn(500);
    });
});

$(document).on('click', '.button_all[title="Главный администратор"]',
    function () {
        $('.bl_table_catalog').fadeOut(500, function () {
            $('.bl_super_user').fadeIn(500);
        });
    });

$(document).on('click', '.act-save', function () {
    var requieredEmpty = false;
    requiredFields.forEach(function (item, i) {
        if ($('#' + item).length !== 0) {
            if ($('#' + item).val().length === 0) {
                requieredEmpty = true;
            }
        }
    });
    if (requieredEmpty) {
        alertify.error('Заполните все обязательные поля!');
    } else {
        $(this).closest('form').submit();
    }
    return false;
});

$(document).on('click', '.act-back', function () {
    $('.bl_redactirovat').fadeOut(500, function () {
        $('.bl_table_catalog').fadeIn(500);
    });
});
