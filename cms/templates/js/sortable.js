var enableSortable = function () {
    var sortable = $('#itemsList tbody');
    sortable.sortable({
        placeholder: "ui-state-highlight",
        stop: stopSort
    });
    sortable.disableSelection();
    alertify.success('Теперь вы можете использовать сортировку!');
};

var disableSortable = function () {
    var sortable = $('#itemsList tbody');
    sortable.enableSelection();
    sortable.sortable("destroy");
    alertify.log('Сортировка отключена!');
};

var stopSort = function () {
    var items = [];
    $(this).find('tr').each(function (i, item) {
        items.push($(item).attr('data-id'));
    });
    refreshSort(items);
};

var refreshSort = function (items) {
    $.ajax({
        url: '',
        data: {
            request: 'refresh',
            'sortItems[]': items
        },
        success: function (data) {
            if (data) {
                alertify.success('Порядок успешно изменён!');
            }
        }
    });
};

$('.act-sort-on').on('click', function () {
    enableSortable();
    $(this).hide();
    $('.act-sort-off').show();
});
$('.act-sort-off').on('click', function () {
    disableSortable();
    $(this).hide();
    $('.act-sort-on').show();
});
