export default class ProductsGridController {
  constructor(ProductsService) {
    this.page = 0;
    this.productsService = ProductsService;
    this.loading = true;
    this.products = [];
  }

  $onInit() {
    this.loading = true;
    this.productsService
      .getProducts()
      .then(response => this.processRequest(response));
  }

  showMore() {
    this.loading = true;
    this.productsService
      .getMore(this.page)
      .then(response => this.processRequest(response));
  }

  processRequest(response) {
    if (response.data === '0') {
      this.loading = false;
      this.noMoreProducts = true;
      return;
    }

    this.products = [...this.products, ...response.data];
    this.loading = false;
    this.page = this.page + 1;
  }
}
