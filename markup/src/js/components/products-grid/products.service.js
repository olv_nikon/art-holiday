export class ProductsService {
  constructor($http, DEFAULT_PRODUCTS_NUM, ITEMS_PER_ROW) {
    this.http = $http;
    this.defaultProductsNum = DEFAULT_PRODUCTS_NUM;
    this.itemsPerRow = ITEMS_PER_ROW;
  }

  getProducts(page = 0, limit = this.defaultProductsNum) {
    const baseUrl = 'ajax_products?request=get_products';
    const url = `${baseUrl}&page_num=${page}&limit=${limit}`;
    return this.http.get(url);
  }

  getMore(page = 1) {
    return this.getProducts(page, this.itemsPerRow);
  }
}
