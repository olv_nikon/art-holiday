import angular from 'angular';
import { productsGridComponent } from './products-grid.component';
import { ProductsService } from './products.service';

export const appProductsGrid = angular.module('app.productsGrid', [])
  .component('productsGrid', productsGridComponent)
  .service('ProductsService', ProductsService);
