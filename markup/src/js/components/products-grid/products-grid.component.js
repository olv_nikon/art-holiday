import template from './products-grid.component.html';
import controller from './products-grid.controller';
import './products-grid.component.less';

export const productsGridComponent = { controller, template };
