import angular from 'angular';
import { appProduct } from './product';
import { appProductsGrid } from './products-grid';

export const componentsModule = angular.module('components', [
  appProduct.name,
  appProductsGrid.name
]);
