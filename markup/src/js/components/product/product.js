import angular from 'angular';
import { productComponent } from './product.component';

export const appProduct = angular.module('app.product', [])
  .component('product', productComponent);
