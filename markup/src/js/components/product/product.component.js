import template from './product.component.html';
import controller from './product.controller';
import './product.component.less';

export const productComponent = {
  bindings: {
    product: '<'
  },
  controller,
  template
};
