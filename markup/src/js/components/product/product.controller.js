export default class ProductController {
  constructor(CartService) {
    this.cartService = CartService;
    this.amount = 30;
    this.withHiFloat = false;
    this.hiFloatDescription = 'Специальное вещество,  которое позволяет шарику держаться дольше';
  }

  addOne() {
    this.amount += 1;
  }

  subtractOne() {
    if (this.amount === 0) {
      return;
    }
    this.amount -= 1;
  }

  putToCart() {
    const product = Object.assign({}, this.product);
    product.amount = this.amount;
    product.totalPrice = this.totalPrice;
    product.withHiFloat = this.withHiFloat;
    this.isInCart = true;
    this.cartService.putToCart(product);
  }

  openCart() {
    this.cartService.openCart();
  }

  get totalPrice() {
    return (this.amount * Number(this.product.price)).toFixed(2);
  }
}
