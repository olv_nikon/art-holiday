import angular from 'angular';
import 'angular-ui-bootstrap';
import { commonModule } from './common';
import { componentsModule } from './components';
import {
  DEFAULT_PRODUCTS_NUM,
  ITEMS_PER_ROW,
} from './constants';

angular.module('app', [
  'ui.bootstrap',
  commonModule.name,
  componentsModule.name,
]).constant('DEFAULT_PRODUCTS_NUM', DEFAULT_PRODUCTS_NUM)
  .constant('ITEMS_PER_ROW', ITEMS_PER_ROW);

angular.bootstrap(document, ['app']);
