import template from './cart.component.html';
import controller from './cart.controller';
import './cart.component.less';

export const cartComponent = {
  controller,
  template
};
