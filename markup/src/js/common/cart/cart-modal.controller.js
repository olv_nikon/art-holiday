import './cart-modal.less';

export class CartModalController {
  constructor(products, $uibModalInstance) {
    this.uibModalInstance = $uibModalInstance;
    this.products = products;

    this.orderTimeFrom = new Date();
    this.orderTimeFrom.setHours(8, 0, 0, 0);

    this.orderTimeTo = new Date();
    this.orderTimeTo.setHours(9, 0, 0, 0);

    this.orderDate = new Date();
    this.dateOptions = {
      formatYear: 'yy',
      maxDate: new Date(2020, 5, 22),
      minDate: new Date(),
      startingDay: 1
    };
  }

  setOrderDate() {
    this.orderDateOpened = true;
  }

  ok() {
    this.uibModalInstance.close();
  }

  cancel() {
    this.uibModalInstance.dismiss('cancel');
  }
}
