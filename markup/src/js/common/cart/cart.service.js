import { BehaviorSubject } from 'rxjs';
import { CartModalController } from './cart-modal.controller';
import modalTemplate from './cart-modal.template.html';

export class CartService {
  constructor($http, $uibModal) {
    this.http = $http;
    this.uibModal = $uibModal;
    this.products = new BehaviorSubject([]);
  }

  putToCart(product) {
    this.products.next([...this.products.getValue(), product]);
  }

  openCart() {
    const productsInCart = [...this.products.getValue()];
    this.uibModal.open({
      template: modalTemplate,
      controller: CartModalController,
      controllerAs: '$ctrl',
      resolve: {
        products: () => productsInCart,
      },
    }).result.then(() => {}, () => {});
  }

  get productsInCart() {
    return this.products.asObservable();
  }
}
