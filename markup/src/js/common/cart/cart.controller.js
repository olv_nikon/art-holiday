export default class CartController {
  constructor(CartService) {
    this.cartService = CartService;
    this.productsInCart = [];
  }

  $onInit() {
    this.cartService
      .productsInCart
      .subscribe(products => (this.productsInCart = products));
  }

  openCart() {
    this.cartService.openCart();
  }
}
