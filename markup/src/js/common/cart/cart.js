import angular from 'angular';
import { CartService } from './cart.service';
import { cartComponent } from './cart.component';

export const cartModule = angular.module('common.cart', [])
  .service('CartService', CartService)
  .component('cart', cartComponent);
