export default class BannerController {
  constructor(BannerService, $sce, $filter, $interval) {
    this.sce = $sce;
    this.filter = $filter;
    this.interval = $interval;
    this.BannerService = BannerService;
    this.activeBanner = {};
  }

  $onInit() {
    this.BannerService
      .getActiveBanner()
      .then((response) => {
        const data = response.data;
        if (!data) return;

        this.activeBanner = Object.assign({}, data);
      });

    this.bannerLastDay = this.nextMondayDate;
  }

  get nextMondayDate() {
    const today = new Date();
    const date = today.getDate();
    const day = today.getDay();
    const daysInWeek = 7;
    const dayAfterSunday = daysInWeek + 1;
    const nextMondayDay = date + (dayAfterSunday - day) % daysInWeek;
    const nextMondayDate = (new Date()).setDate(nextMondayDay);
    return this.filter('date')(nextMondayDate, 'yyyy-MM-dd');
  }

  get bannerContent() {
    return this.sce.trustAsHtml(this.activeBanner.content);
  }
}
