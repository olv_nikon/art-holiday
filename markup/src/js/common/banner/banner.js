import angular from 'angular';
import { bannerComponent } from './banner.component';
import { BannerService } from './banner.service';

export const bannerModule = angular.module('common.banner', [])
  .component('banner', bannerComponent)
  .service('BannerService', BannerService);
