export class BannerService {
  constructor($http) {
    this.http = $http;
  }

  getActiveBanner() {
    return this.http.get('ajax_banner?request=get_banner');
  }
}
