import template from './banner.component.html';
import controller from './banner.controller';
import './banner.component.less';

export const bannerComponent = {
  controller,
  template
};
