import '../../../../../node_modules/fancybox/dist/css/jquery.fancybox.css';
import '../../../../../node_modules/fancybox/dist/js/jquery.fancybox';

export function Fancybox() {
  return {
    restrict: 'A',
    link: ($scope, $element, $attrs) => {
      const link = $element.find('a');
      link.attr('data-fancybox-group', $attrs.fancybox || 'fancybox');

      link.fancybox({
        closeBtn: false,
        helpers: {
          title: {
            type: 'inside'
          },
          buttons: {},
          media: {},
        },
      });
    }
  }
}
