import '../../../../../node_modules/fancybox/dist/css/jquery.fancybox.css';
import '../../../../../node_modules/fancybox/dist/js/jquery.fancybox';

export function FancyboxYoutube() {
  return {
    restrict: 'A',
    link: ($scope, $element, $attrs) => {
      const link = $element.find('a');
      link.attr('data-fancybox-group', $attrs.fancyboxYoutube || 'fancybox');

      link.click(function () {
        $.fancybox({
          padding: 0,
          autoScale: false,
          transitionIn: 'none',
          transitionOut: 'none',
          title: this.title,
          width: 680,
          height: 495,
          href: this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
          type: 'swf',
          swf: {
            wmode: 'transparent',
            allowfullscreen: 'true'
          }
        });

        return false;
      });
    }
  }
}
