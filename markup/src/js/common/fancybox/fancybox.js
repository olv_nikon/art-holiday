import angular from 'angular';
import { Fancybox } from './fancybox.directive';
import { FancyboxYoutube } from './fancybox-youtube.directive';

export const fancyboxModule = angular.module('common.fancybox', [])
  .directive('fancybox', Fancybox)
  .directive('fancyboxYoutube', FancyboxYoutube);
