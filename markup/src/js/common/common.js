import angular from 'angular';
import { flipTimerModule } from './flip-timer';
import { slickCarouselModule } from './slick-carousel';
import { fancyboxModule } from './fancybox';
import { vkCommentsModule } from './vk-comments';
import { webuiPopoverModule } from './webui-popover';
import { cartModule } from './cart';
import { scrollSpyModule } from './scroll-spy';
import { bannerModule } from './banner';

export const commonModule = angular.module('common', [
  flipTimerModule.name,
  slickCarouselModule.name,
  fancyboxModule.name,
  vkCommentsModule.name,
  webuiPopoverModule.name,
  cartModule.name,
  scrollSpyModule.name,
  bannerModule.name,
]);
