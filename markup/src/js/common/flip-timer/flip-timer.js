import angular from 'angular';
import { FlipTimer } from './flip-timer.directive';

export const flipTimerModule = angular.module('common.flipTimer', [])
  .directive('flipTimer', FlipTimer);
