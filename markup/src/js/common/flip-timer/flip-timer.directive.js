import '../../../../../node_modules/flipclock/compiled/flipclock.css';
import '../../../../../node_modules/flipclock/compiled/flipclock';

export function FlipTimer() {
  return {
    restrict: 'A',
    link: ($scope, $element, $attrs) => {
      const getDiff = () => {
        const futureDate  = new Date($attrs.flipTimer);
        return (futureDate.getTime() - new Date()) / 1000;
      };

      const flipClock = $element.FlipClock(getDiff(), {
        clockFace: 'DailyCounter',
        countdown: true,
        language: 'russian'
      });

      $attrs.$observe('flipTimer', () => {
        flipClock.time.time = getDiff();
      });
    }
  }
}
