import angular from 'angular';
import { webuiPopover } from './webui-popover.directive';

export const webuiPopoverModule = angular.module('common.webuiPopover', [])
  .directive('webuiPopover', webuiPopover);
