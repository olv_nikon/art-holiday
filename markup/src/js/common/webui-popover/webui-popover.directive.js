import '../../../../../node_modules/webui-popover/dist/jquery.webui-popover.min.css';
import '../../../../../node_modules/webui-popover/dist/jquery.webui-popover.min';

export function webuiPopover() {
  return {
    link: ($scope, $element, $attrs) => {
      $element.webuiPopover({
        animation: "pop",
        trigger: "hover",
        delay: {
          show: 300,
          hide: 300
        },
        placement: $attrs.placement || "bottom",
      });
    },
    restrict: 'A',
  }
}
