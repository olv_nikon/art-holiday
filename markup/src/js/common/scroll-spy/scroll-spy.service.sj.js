import { BehaviorSubject } from 'rxjs';

export class ScrollSpyService {
  constructor($window) {
    this.scrollPosition = new BehaviorSubject(this.documentScrollPosition);

    let timer;
    $window.onscroll = () => {
      if (timer) {
        $window.clearTimeout(timer);
        timer = undefined;
      }

      timer = $window.setTimeout(() => {
        this.scrollPosition.next(this.documentScrollPosition);
      }, 0);
    };
  }

  get currentScrollPosition() {
    return this.scrollPosition.asObservable();
  }

  get documentScrollPosition() {
    return document.documentElement.scrollTop || document.body.scrollTop;
  }
}
