export function ScrollSpy(ScrollSpyService) {
  return {
    restrict: 'A',
    link: ($scope, $element, $attrs) => {
      const scrolledClass = $attrs.scrollSpy;
      const triggerOnHeight = $attrs.triggerOnHeight;
      const height = $element.innerHeight();
      const spacer = $('<div>').css({
        height: `${height}px`,
        width: '100%',
        display: 'none'
      });
      $element.parent().prepend(spacer);

      ScrollSpyService
        .currentScrollPosition
        .subscribe(position => {
          const hasCrossedLine = position >= triggerOnHeight;
          $element.toggleClass(scrolledClass, hasCrossedLine);
          spacer.toggle(hasCrossedLine);
        });
    },
  }
}
