import angular from 'angular';
import { ScrollSpyService } from './scroll-spy.service.sj';
import { ScrollSpy } from './scroll-spy.directive';

export const scrollSpyModule = angular.module('common.scrollSpy', [])
  .service('ScrollSpyService', ScrollSpyService)
  .directive('scrollSpy', ScrollSpy);
