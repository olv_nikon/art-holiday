import angular from 'angular';
import { vkComments } from './vk-comments.directive';

export const vkCommentsModule = angular.module('common.vkComments', [])
  .directive('vkComments', vkComments);
