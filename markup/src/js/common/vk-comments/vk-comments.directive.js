import template from './vk-comments.directive.html';
import controller from './vk-comments.controller';
import './vk-comments.directive.less';

export function vkComments() {
  return {
    template,
    controller,
    link: ($scope, $element, $attrs, $ctrl) => {
      const blockId = `${$ctrl.blockName}_${$ctrl.apiId}`;
      const vk = $ctrl.window.VK;
      vk.init({ apiId: $ctrl.apiId, onlyWidgets: true });
      vk.Widgets.Comments(blockId, { limit: 5, attach: "*" });
    },
    scope: { apiId: "@" },
    bindToController: true,
    controllerAs: "$ctrl",
    restrict: 'E',
  }
}
