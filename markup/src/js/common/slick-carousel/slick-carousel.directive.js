import '../../../../../node_modules/slick-carousel/slick/slick.css';
import '../../../../../node_modules/slick-carousel/slick/slick-theme.css';
import '../../../../../node_modules/slick-carousel/slick/slick';

export function SlickCarousel() {
  return {
    restrict: 'A',
    link: ($scope, $element) => {
      $element.slick({
        infinite: true,
        slidesToShow: 6,
        slidesToScroll: 3
      });
    }
  }
}
