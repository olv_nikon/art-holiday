import angular from 'angular';
import { SlickCarousel } from './slick-carousel.directive';

export const slickCarouselModule = angular.module('common.slickCarousel', [])
  .directive('slickCarousel', SlickCarousel);
