const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');

module.exports = () => {
  const distRoot = path.resolve(__dirname, 'dist/');

  return {
    entry: path.resolve(__dirname, 'src/index.js'),
    output: {
      path: distRoot,
      filename: './js/bundle.js'
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          use: ['babel-loader?cacheDirectory=true'],
          exclude: /node_modules/
        },
        {
          test: /(\.less$|\.css$)/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: ['css-loader', 'less-loader']
          })
        },
        {
          test: /\.html$/,
          use: 'html-loader'
        },
        {
          test: /\.(jpg|png|gif)$/,
          use: {
            loader: 'file-loader',
            options: {
              outputPath: 'img/',
              publicPath: './'
            }
          }
        },
        {
          test: /\.(eot|woff2?|svg|ttf)([\?]?.*)$/,
          use: {
            loader: 'file-loader',
            options: {
              outputPath: 'css/',
              publicPath: '../'
            }
          }
        }
      ]
    },
    plugins: [
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
        'window.$': 'jquery',
      }),
      new ExtractTextPlugin('css/styles.css'),
      new HtmlWebpackPlugin({
        template: 'src/template.html',
        inject: 'body',
        filename: 'index.html'
      }),
      new CopyWebpackPlugin([{ from: 'src/img', to: 'img' }]),
    ],
    devServer: {
      contentBase: path.join(__dirname, 'dist'),
      compress: true,
      port: 3000
    }
  }
};
